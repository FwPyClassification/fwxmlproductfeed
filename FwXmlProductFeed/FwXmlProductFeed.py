''' XML Product Feed Converter
The purpose of this converter is taking products feeds in XML format and
converting them into 2D tables of data to be used in Excel and the system.

For XML attributes that have multiple values within a given product, we
want to create enough columns to hold each value and reconcile them later.

To start with, we count the maximum number of product values for any single
attribute, then create field headers for each.

Then we populate a temporary list array for each row according to the
fieldname and how many times the row has that value. For example if the
row has two "Feature" values, it would put those values in the array column.
"Feature" and "Feature (1)"
Then it writes the list array as a CSV row.


Changes/refinements:
    make it more flexible if the list of products varies in depth/name in the XML tree
        currently I have to manually remove extra layers so the list of products in right below the root element.

    make it more robust in handling character sets/no-compliant XML characters

    make it more "pythonic"
        this was my first python project so it isn't elegant or pythonic.

    switch it to use pandas dataframe object
        as long as there isn't a significant memory/speed penalty.

    switch it to output .xlsx file instead of csv (or have the option of either)

'''
import sys, time, csv
from lxml import etree as lET
import pandas as pd
import FwUtil
import FwUI
import FwMisc
from collections import defaultdict, Counter

def main():
    """ Main conversion function """

    #Prompt the user to select the XML file to run
    s_xml_file = FwUI.get_open_filename(['xml'], 'xml', "Select the XML Product Feed to Convert")
    if len(s_xml_file) == 0:
        sys.exit("No XML file selected.")

    e_export_types = {'CSV (*.csv)':'.csv', 'Excel (*.xlsx)':'.xlsx', 'Panda Dataframe (*.pkl)':'.pkl'}
    a_export_type = FwUI.get_selected_list_items(list(e_export_types.keys()), s_title="Select Export Type")
    if len(a_export_type) == 0:
        sys.exit("No export type selected.")

    s_export_file = FwMisc.folder_from_path(s_xml_file) + FwMisc.file_from_path(s_xml_file) + e_export_types[a_export_type[0]]

    n_start = time.time()

    #get the root element object of the xml file
    o_root = get_xml_root(s_xml_file)

    #collect field information
    e_xml_field_col, e_fields_col, a_fields_ordered = get_fields(o_root)

    pd_feed = get_dataframe(o_root, e_xml_field_col, e_fields_col, a_fields_ordered)

    if e_export_types[a_export_type[0]] == '.csv':
        pd_feed.to_csv(s_export_file)
    elif e_export_types[a_export_type[0]] == '.xlsx':
        o_writer = pd.ExcelWriter(s_export_file, engine='xlsxwriter', options={'strings_to_urls': False})
        pd_feed.to_excel(o_writer, index=False,)
    elif e_export_types[a_export_type[0]] == '.pkl':
        pd_feed.to_pickle(s_export_file)

    print("XML-Excel Conversion finished in %.2f seconds." % (time.time() - n_start))
    

def get_xml_root(s_xml_file=None):

    #from my experiments, python 2.7 doesn't support the "recover" parameter
    #and will crash on XML files with non-XML supported characters.
    #Sometimes datasets that are labelled in the file as Unicode are really some other character-set
    o_parser = lET.XMLParser(recover=True)
    o_element_tree = lET.parse(s_xml_file, parser=o_parser)

    # get the XML tree root
    return o_element_tree.getroot()


def get_fields(o_root):
    #find all the possible fields for the XML attributes and text
    e_counts, e_values = get_attribute_counts(o_root)

    #list of the fields in order of first appearance
    a_values = sorted(e_values, key=e_values.get)

    #Remove the namespace from the fields, keeping just the field name
    e_fields = defaultdict(int) # unique field names with associated column number
    a_fields = [] # fields in correct order

    l_next_col = 0
    for s_field in a_values:
        if "}" in s_field: #remove extraneous xml info from field name
            s_key = s_field.split('}', 1)[1]
        else:
            s_key = s_field

        #the original header
        a_fields.append(s_key)
        e_fields[s_key] = l_next_col
        l_next_col += 1

        if e_counts[s_field] > 1: # the 2nd through nth headers
            for l_field in range(2, e_counts[s_field] + 1):
                s_newkey = "%s (%s)" % (s_key, l_field)
                a_fields.append(s_newkey)
                e_fields[s_newkey] = l_next_col
                l_next_col += 1

        e_values[s_field] = s_key

    print(a_fields)  #print the list of fields to the command line
    print('\n')
    return (e_values, e_fields, a_fields)


def get_attribute_counts(o_products):
    """ find all the possible fields for the XML attributes and text """
    e_counts = {} #dict of field names (attributes/values) with a column count for each
    e_values = {} #dict of field names (attributes/values) with a unique column number for each

    l_next_col = 0
    l_rows = 0

    for o_row in o_products:
        l_rows = l_rows + 1

        #count of how many times attributes/values appear in this product (used for creating multiple columns)
        e_row = {}

        #get the row's attributes
        for s_key in o_row.attrib.keys():
            if not s_key in e_values:
                e_values[s_key] = l_next_col
                l_next_col += 1
                e_counts[s_key] = 1
            else:
                if not s_key in e_row:
                    e_row[s_key] = 1
                else:
                    e_row[s_key] += 1
                e_counts[s_key] = max(e_counts[s_key], e_row[s_key])

        #get the row's child values
        o_values = o_row.getiterator()
        for val in o_values:
            s_key = val.tag
            if not s_key in e_values:
                e_values[s_key] = l_next_col
                l_next_col += 1
                e_counts[s_key] = 1
            else:
                if not s_key in e_row:
                    e_row[s_key] = 1
                else:
                    e_row[s_key] += 1
                e_counts[s_key] = max(e_counts[s_key], e_row[s_key])
    print("%s Rows" % l_rows) #print the number of row to the command line
    return (e_counts, e_values)

    
def get_dataframe(o_root, e_xml_field_col, e_fields_col, a_fields_ordered):
    a_table = []
    i_col_count = len(e_fields_col)

    #write all the rows with the same number and order of fields
    for o_row in o_root:
        a_row = [None] * i_col_count

        #count of times attributes/values appear in one product (used for creating multiple columns)
        e_row_counts = defaultdict(int)

        #the row attributes
        for s_key in o_row.attrib.keys():
            a_row[e_fields_col[s_key]] = o_row.attrib[s_key]

        #the row values
        o_values = o_row.getiterator()
        for val in o_values:
            s_key = e_xml_field_col[val.tag]
            e_row_counts[s_key] += 1 #count of particular attribute in row

            if e_row_counts[s_key] > 1:
                s_key = "%s (%s)" % (s_key, e_row_counts[s_key])

            if not val.text is None:
                try:
                    a_row[e_fields_col[s_key]] = val.text
                except:
                    print("%s, %s" % (val.text, type(val.text))) #print to the console for debugging
                    a_row[e_fields_col[s_key]] = val.text

        #write out the row
        a_table.append(a_row)
        
    pd_rows = pd.DataFrame(a_table, columns=a_fields_ordered)
    return pd_rows

if __name__ == "__main__":
    main()

    